from django.test import TestCase
from unittest.mock import MagicMock

from CIResults.templatetags.helpers import list_custom_fields, issues_list_custom_fields, lookup


class list_custom_fieldsTests(TestCase):
    def test_basic_scenario(self):
        bug1 = MagicMock(custom_fields={'hello': 'world'})
        bug2 = MagicMock(custom_fields={'foo': 'bar'})

        self.assertEqual(list_custom_fields([bug1, bug2]), ['foo', 'hello'])


class issues_list_custom_fieldsTests(TestCase):
    def test_basic_scenario(self):
        issue1 = MagicMock(bugs_cached=[MagicMock(custom_fields={'toto': 'tata'}),
                                        MagicMock(custom_fields={'42': 'answer to life the universe and everything'})])
        issue2 = MagicMock(bugs_cached=[MagicMock(custom_fields={'hello': 'world'}),
                                        MagicMock(custom_fields={'foo': 'bar'})])

        self.assertEqual(issues_list_custom_fields([issue1, issue2]), ['42', 'foo', 'hello', 'toto'])


class lookupTestsTests(TestCase):
    def test_when_key_exists(self):
        obj = {'hello': 'world'}
        self.assertEqual(lookup(obj, 'hello'), 'world')

    def test_when_key_doesnt_exist(self):
        self.assertEqual(lookup({}, '42'), None)
