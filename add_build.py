#!/usr/bin/env python3

import django
import argparse
import sys

django.setup()

from CIResults.run_import import BuildResult  # noqa

# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("build_dir", help="Directory containing the build information", nargs='?')

parser.add_argument("-n", "--name", help="Name of the build. Has to be unique")
parser.add_argument("-c", "--component", help="Component from which this build is")
parser.add_argument("-v", "--version", help="Version of this build (git sha1, revision number, ...)")
parser.add_argument("-b", "--branch", help="Branch this version is from")
parser.add_argument("-r", "--repo", help="Repository this version is from")
parser.add_argument("-u", "--url", help="HTTP URL showing this commit/version")
parser.add_argument("-f", "--build_flags", help="Parameters used to compile this build")
parser.add_argument("-C", "--config_file",
                    help="File containing the parameters used to compile this build (can be .bz2)")
parser.add_argument("-B", "--build_log_file", help="File containing the build log (can be .bz2)")
parser.add_argument("-p", "--parent", dest="parents", type=str, action='append',
                    help="Build name from the same component")

args = parser.parse_args()

# Get the component
build = BuildResult(build_dir=args.build_dir, name=args.name,
                    component=args.component, version=args.version,
                    branch=args.branch, repo=args.repo, upstream_url=args.url,
                    parameters=args.build_flags,
                    config_file=args.config_file,
                    build_log_file=args.build_log_file, parents=args.parents)
build.commit_to_db()

sys.exit(0)
