#!/bin/bash

docker run --rm -w /app --env-file /etc/cibuglog.env -ti registry.freedesktop.org/gfx-ci/cibuglog:latest $@
